﻿using UnityEngine;

namespace DELTation.DIFramework.Containers
{
	public interface IIgnoreByContainer { }

	public sealed class IgnoreByContainer : MonoBehaviour, IIgnoreByContainer { }
}